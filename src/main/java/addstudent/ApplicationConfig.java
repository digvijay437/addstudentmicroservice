package addstudent;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {
	
	private static final Logger logger= LoggerFactory.getLogger(ApplicationConfig.class);
	private Map<Integer, Student> studentMap = new HashMap<Integer, Student>();
	
	@PostConstruct
	public void addNewStudent(){
		
		logger.info("Adding Student in student map");
		Student student1 = new Student(1001, "Digvijay Singh", "Maths");
		Student student2 = new Student(1002, "Vivek Anand", "Science");
		Student student3 = new Student(1003, "Prabhat Sharms", "Arts");
		Student student4 = new Student(1004, "Rimsha Naaz", "Biology");
		logger.info("Adding student with name: {}", student1.getName());
		studentMap.put(student1.getId(), student1);
		logger.info("Adding student with name: {}", student2.getName());
		studentMap.put(student2.getId(), student2);
		logger.info("Adding student with name: {}", student3.getName());
		studentMap.put(student3.getId(), student3);
		logger.info("Adding student with name: {}", student4.getName());
		studentMap.put(student4.getId(), student4);
		logger.info("Finished adding students");
	}

	public Map<Integer, Student> getStudentMap() {
		return studentMap;
	}

}
