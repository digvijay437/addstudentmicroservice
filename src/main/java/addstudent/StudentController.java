package addstudent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class StudentController {
	
	private static final Logger logger = LoggerFactory.getLogger(StudentController.class);
	
	@Autowired
	private AddStudentService service;
	
	@RequestMapping(value="/addstudent", method=RequestMethod.POST)
	public Student addStudent(@RequestBody Student student){
		logger.info("Calling Service class for student addition");
		Student student1 = service.addStudent(student);
		return student1;
	}

}
