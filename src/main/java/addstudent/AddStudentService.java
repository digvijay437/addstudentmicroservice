package addstudent;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddStudentService {
	
	private static final Logger logger = LoggerFactory.getLogger(AddStudentService.class);
	
	@Autowired
	private ApplicationConfig appconfig;
	
	public Student addStudent(Student student){
		Map<Integer, Student> studentMap = appconfig.getStudentMap();
		logger.info("Adding student with name: {}", student.getName());
		studentMap.put(student.getId(), student);
		logger.info("Finished adding student");
		logger.info("Length of student map is: {}", studentMap.size());
		return student;
	}

}
